package com.example.android.find_your_flat;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by andronov on 21-May-17.
 */

public class ResultListAdapter extends ArrayAdapter<Property> {

    Context context;
    int layoutResourceId;
    Property data[] = null;

    public ResultListAdapter(Context context, int layoutResourceId, Property[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ResultHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ResultHolder();
            holder.imgIcon = (ImageView)row.findViewById(R.id.item_img);
            holder.txtTitle = (TextView)row.findViewById(R.id.name);
            holder.txtType = (TextView)row.findViewById(R.id.type);
            holder.txtSize = (TextView)row.findViewById(R.id.size);
            holder.txtAddress = (TextView)row.findViewById(R.id.address);

            row.setTag(holder);
        }
        else
        {
            holder = (ResultHolder)row.getTag();
        }

        Property property = data[position];
        holder.txtTitle.setText(property.name);
        holder.imgIcon.setImageResource(property.item_img);
        holder.txtType.setText(property.type);
        holder.txtSize.setText(property.size);
        holder.txtAddress.setText(property.address);

        return row;
    }

    static class ResultHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
        TextView txtType;
        TextView txtSize;
        TextView txtAddress;
    }

}
